package com.pratamawijaya.androidtddexample;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by pratama
 * Date : Jul - 7/15/17
 * Project Name : AndroidTDDExample
 */
public class LoginPresenterTest {

    private LoginView loginView;
    private LoginPresenter loginPresenter;

    @Before
    public void setUp() throws Exception {
        loginView = mock(LoginView.class);
        loginPresenter = new LoginPresenter(loginView);
    }

    @Test
    public void checkIfLoginAttemptIsExceeded() throws Exception {
        Assert.assertEquals(1, loginPresenter.incrementLoginAttempt());
        Assert.assertEquals(2, loginPresenter.incrementLoginAttempt());
        Assert.assertEquals(3, loginPresenter.incrementLoginAttempt());

        Assert.assertTrue(loginPresenter.isLoginAttemptExceeded());
    }

    @Test
    public void checkIfLoginAttemptIsNotExceeded() throws Exception {
        Assert.assertFalse(loginPresenter.isLoginAttemptExceeded());
    }

    @Test
    public void checkUsernamePasswordIsCorrect() throws Exception {
        loginPresenter.doLogin("tama", "passtama");
        verify(loginView).showLoginSuccessMessage();
    }

    @Test
    public void checkUsernamePasswordIncorrect() throws Exception {
        loginPresenter.doLogin("a", "a");
        verify(loginView).showErrorMessageForUsernamePassword();
    }

    @Test
    public void checkLoginAttemptIncorrectPass() throws Exception {
        loginPresenter.doLogin("a", "a");
        checkLoginAttempt();
        loginPresenter.doLogin("a", "a");
        checkLoginAttempt();
        loginPresenter.doLogin("a", "a");
        checkLoginAttempt();

        loginPresenter.doLogin("a", "a");
        verify(loginView).showErrorMessageForLoginAttempt();
    }

    private void checkLoginAttempt() {
        System.out.println("login attempt " + loginPresenter.getLoginAttempt());
    }
}