package com.pratamawijaya.androidtddexample.data;

import com.pratamawijaya.androidtddexample.data.model.Badge;
import com.pratamawijaya.androidtddexample.data.model.User;
import com.pratamawijaya.androidtddexample.data.model.UserStat;
import com.pratamawijaya.androidtddexample.data.response.BadgeResponse;
import com.pratamawijaya.androidtddexample.data.response.UserResponse;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.anyInt;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */
public class UserServicesTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    StackoverflowServices stackoverflowServices;

    @InjectMocks
    UserServices userServices;

    @Test
    public void test_loaduser_return_3_user() throws Exception {
        Mockito.when(stackoverflowServices.getTopUsers()).thenReturn(
                Single.just(getMockUserResponse())
        );

        Mockito.when(stackoverflowServices.getBadges(anyInt())).thenReturn(
                Single.just(getMockBadgeResponse())
        );

        TestObserver<List<UserStat>> testObserver = userServices.loadUsers().test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors()
                .assertValue(value -> value.size() == 3);

    }

    private BadgeResponse getMockBadgeResponse() {
        Badge badge1 = new Badge.Builder()
                .name("Badge 1")
                .build();
        List<Badge> listBadge = new ArrayList<>();
        listBadge.add(badge1);
        BadgeResponse badgeResponse = new BadgeResponse();
        badgeResponse.setItems(listBadge);
        return badgeResponse;
    }

    private UserResponse getMockUserResponse() {
        User user1 = new User.Builder()
                .id(1)
                .name("User 1")
                .location("USA")
                .build();

        User user2 = new User.Builder()
                .id(1)
                .name("User 2")
                .location("USA")
                .build();

        User user3 = new User.Builder()
                .id(3)
                .name("User 3")
                .location("USA")
                .build();
        List<User> listUser = new ArrayList<>();
        listUser.add(user1);
        listUser.add(user2);
        listUser.add(user3);

        UserResponse userResponse = new UserResponse.Builder()
                .items(listUser)
                .build();

        return userResponse;
    }
}