package com.pratamawijaya.androidtddexample;

/**
 * Created by pratama
 * Date : Jul - 7/15/17
 * Project Name : AndroidTDDExample
 */

public class LoginPresenter {
    private static final int MAX_LOGIN_ATTEMPT = 3;
    private int loginAttempt = 0;
    private LoginView loginView;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    public int incrementLoginAttempt() {
        loginAttempt++;
        return loginAttempt;
    }

    public boolean isLoginAttemptExceeded() {
        return loginAttempt >= MAX_LOGIN_ATTEMPT;
    }

    public int getLoginAttempt() {
        return loginAttempt;
    }

    public void doLogin(String user, String pass) {
        if (isLoginAttemptExceeded()) {
            loginView.showErrorMessageForLoginAttempt();
            return;
        }

        if (user.equals("tama") && pass.equals("passtama")) {
            loginView.showLoginSuccessMessage();
            return;
        }

        incrementLoginAttempt();
        loginView.showErrorMessageForUsernamePassword();
    }
}
