package com.pratamawijaya.androidtddexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements LoginView {

    private EditText inputUsername;
    private EditText inputPassword;
    private Button btnLogin;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        presenter = new LoginPresenter(this);
    }

    private void initView() {
        inputUsername = (EditText) findViewById(R.id.inputUsername);
        inputPassword = (EditText) findViewById(R.id.inputPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.doLogin(inputUsername.getText().toString(),
                        inputPassword.getText().toString());
            }
        });
    }

    @Override
    public void showErrorMessageForUsernamePassword() {

    }

    @Override
    public void showErrorMessageForLoginAttempt() {

    }

    @Override
    public void showLoginSuccessMessage() {

    }
}
