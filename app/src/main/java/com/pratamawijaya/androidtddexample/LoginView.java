package com.pratamawijaya.androidtddexample;

/**
 * Created by pratama
 * Date : Jul - 7/15/17
 * Project Name : AndroidTDDExample
 */

public interface LoginView {
    void showErrorMessageForUsernamePassword();

    void showErrorMessageForLoginAttempt();

    void showLoginSuccessMessage();
}
