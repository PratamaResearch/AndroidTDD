package com.pratamawijaya.androidtddexample.data.response;

import com.pratamawijaya.androidtddexample.data.model.User;

import java.util.List;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public class UserResponse {
    private List<User> items;

    private UserResponse(Builder builder) {
        items = builder.items;
    }

    public List<User> getItems() {
        return items;
    }


    public static final class Builder {
        private List<User> items;

        public Builder() {
        }

        public Builder items(List<User> val) {
            items = val;
            return this;
        }

        public UserResponse build() {
            return new UserResponse(this);
        }
    }
}
