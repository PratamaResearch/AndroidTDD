package com.pratamawijaya.androidtddexample.data;

import com.pratamawijaya.androidtddexample.data.response.BadgeResponse;
import com.pratamawijaya.androidtddexample.data.response.UserResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public interface StackoverflowServices {
    @GET("/users")
    Single<UserResponse> getTopUsers();

    @GET("/users/{userId}/badges")
    Single<BadgeResponse> getBadges(@Path("userId") int userId);
}
