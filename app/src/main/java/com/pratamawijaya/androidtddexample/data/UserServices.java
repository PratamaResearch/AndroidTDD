package com.pratamawijaya.androidtddexample.data;

import com.pratamawijaya.androidtddexample.data.model.User;
import com.pratamawijaya.androidtddexample.data.model.UserStat;
import com.pratamawijaya.androidtddexample.data.response.UserResponse;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public class UserServices {
    private StackoverflowServices services;

    public UserServices(StackoverflowServices services) {
        this.services = services;
    }

    public Single<List<UserStat>> loadUsers() {
        return services.getTopUsers()
                .flattenAsObservable(UserResponse::getItems)
                .take(5)
                .flatMapSingle(this::loadUserStat)
                .toList();
    }

    private Single<UserStat> loadUserStat(final User user) {
        return services.getBadges(user.getId())
                .subscribeOn(Schedulers.io())
                .map(badgeResponse -> badgeResponse.getItems())
                .map(badges -> new UserStat.Builder()
                        .user(user)
                        .badges(badges)
                        .build());
    }
}
