package com.pratamawijaya.androidtddexample.data.model;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public class User {
    private int id;
    private String name;
    private String location;

    private User(Builder builder) {
        id = builder.id;
        name = builder.name;
        location = builder.location;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }


    public static final class Builder {
        private int id;
        private String name;
        private String location;

        public Builder() {
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder location(String val) {
            location = val;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
