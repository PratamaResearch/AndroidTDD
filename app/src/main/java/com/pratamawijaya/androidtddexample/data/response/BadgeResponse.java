package com.pratamawijaya.androidtddexample.data.response;

import com.pratamawijaya.androidtddexample.data.model.Badge;

import java.util.List;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public class BadgeResponse {
    private List<Badge> items;

    public List<Badge> getItems() {
        return items;
    }

    public void setItems(List<Badge> items) {
        this.items = items;
    }


}
