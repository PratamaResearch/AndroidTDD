package com.pratamawijaya.androidtddexample.data.model;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public class Badge {
    private String name;

    private Badge(Builder builder) {
        name = builder.name;
    }

    public String getName() {
        return name;
    }


    public static final class Builder {
        private String name;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Badge build() {
            return new Badge(this);
        }
    }
}
