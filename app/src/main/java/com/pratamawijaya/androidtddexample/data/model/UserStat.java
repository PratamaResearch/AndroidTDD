package com.pratamawijaya.androidtddexample.data.model;

import java.util.List;

/**
 * Created by pratama
 * Date : Jul - 7/16/17
 * Project Name : AndroidTDDExample
 */

public class UserStat {
    private User user;
    private List<Badge> badges;

    private UserStat(Builder builder) {
        user = builder.user;
        badges = builder.badges;
    }


    public static final class Builder {
        private User user;
        private List<Badge> badges;

        public Builder() {
        }

        public Builder user(User val) {
            user = val;
            return this;
        }

        public Builder badges(List<Badge> val) {
            badges = val;
            return this;
        }

        public UserStat build() {
            return new UserStat(this);
        }
    }
}
